package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;


public class ShoppingCartTest {

    private ShoppingCartOperation shoppingCartOperation = new ShoppingCart();

    @ParameterizedTest
    @CsvSource({
            "winogrono,  4,  2",
            "czekolada, 3, 1",
            "czekolada, 3, 1",
            "woda, 2, 3",
            "banan, 4, 7",
            "jablko, 2, 5 ",
    })

    void addCorrectProduct(String productName, int price, int quantity){
        assertTrue(shoppingCartOperation.addProducts(productName, price, quantity));
    }

    @ParameterizedTest
    @CsvSource({
            "winogrono,  -4,  2",
            "czekolada, -3, 1",
            "czekolada, 3, 1",
            "woda, -2, -3",
            "jablko, 2, -5 ",
            "jablko, 2, 502 ",
    })

    void addIncorrectProducts(String productName, int price, int quantity){
        assertFalse(shoppingCartOperation.addProducts(productName, price, quantity));
    }

    @ParameterizedTest
    @CsvSource({
            "winogorno,  4, 2, 1",
            "czekolada, 3, 1, 1",
            "woda, 2, 3, 1",
            "banan, 4, 7, 6",
            "jablko, 2, 5, 3",
    })

    public void shouldDeleteProduct(String productName,int price, int quantity, int quantityToDelete){
        //given
            shoppingCartOperation.addProducts(productName, price, quantity);
        //when
            boolean isDeleted = shoppingCartOperation.deleteProducts(productName,quantityToDelete);
        //then
            assertTrue(isDeleted);
    }

    @ParameterizedTest
    @CsvSource({
            "banan, 4, 7, -2",
            "jablko, 2, 5, -5",
            "czekolada, 3, 1, 15",
    })

    public void shouldNotDeleteProduct(String productName,int price, int quantity, int quantityToDelete){
        //given
            shoppingCartOperation.addProducts(productName, price, quantity);
        //when
        boolean isDeleted = shoppingCartOperation.deleteProducts(productName,quantityToDelete);
        //then
        assertFalse(isDeleted);
    }

    @Test
    public void shouldGetCorrectQuantity(){
        //given
            shoppingCartOperation.addProducts("jablko",2,5);
            shoppingCartOperation.addProducts("banan", 4,7);
            shoppingCartOperation.addProducts("czekolada", 3, 1);
            shoppingCartOperation.addProducts("jablko", 2, 5);
        //when
            int quantityOfApples = shoppingCartOperation.getQuantityOfProduct("jablko");
            int quantityOfBananas = shoppingCartOperation.getQuantityOfProduct("banan");
            int quantityOfChocolate = shoppingCartOperation.getQuantityOfProduct("czekolada");
        //then
            assertEquals(10, quantityOfApples);
            assertEquals(7,quantityOfBananas);
            assertEquals(1,quantityOfChocolate);
    }

    @Test
    public void shouldGetCorrectPriceOfAllItems(){
        //given
            shoppingCartOperation.addProducts("jablko",2,5);
            shoppingCartOperation.addProducts("banan", 4,7);
            shoppingCartOperation.addProducts("czekolada", 3, 1);
            shoppingCartOperation.addProducts("jablko", 2, 5);
       //when
            int priceOfAllItems = shoppingCartOperation.getSumProductsPrices();
       //then
            assertEquals(51,priceOfAllItems);
    }

    @ParameterizedTest
    @CsvSource({
            "winogrono,  4,  2",
            "czekolada, 3, 1",
            "czekolada, 3, 1",
            "woda, 2, 3",
            "banan, 4, 7",
            "jablko, 2, 5 ",
    })
    void shouldGetCorrectPrice(String productName, int price, int quantity){
        //given
            shoppingCartOperation.addProducts(productName,price,quantity);
        //when
            int priceOfItem = shoppingCartOperation.getProductPrice(productName);
        //then
            assertEquals(price,priceOfItem);
    }

}
